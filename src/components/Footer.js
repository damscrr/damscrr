import React from "react";
import {faGithub, faGitlab, faLinkedin, faTwitter} from "@fortawesome/free-brands-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function Footer() {
    return (
        <div className="footer">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <p>Contactez moi par mail :</p>
                        <div className="d-flex">
                            <a href="mailto:damien.carrier95@gmail.com">damien.carrier95@gmail.com</a>
                        </div>
                        <div className="d-flex">
                            <a href="mailto:damien.carrier@mines-ales.org">damien.carrier@mines-ales.org</a>
                        </div>
                    </div>
                    <div className="col">
                        <p className="pt-3 text-center">
                            Copyright&copy;
                            {new Date().getFullYear()}&nbsp;Damien Carrier | All Rights Reserved
                        </p>
                    </div>
                    <div className="col footer-social">
                        <div>
                            <a href="https://twitter.com/MirakuSan" target="_blank" className="footer-icon">
                                <FontAwesomeIcon icon={faTwitter} size="2x"/> Twitter
                            </a>
                            <a href="https://www.linkedin.com/in/damien-carrier-b73005150/" target="_blank">
                                <FontAwesomeIcon icon={faLinkedin} size="2x"/> Linkedin
                            </a>
                        </div>
                        <div className="footer-git">
                            <a href="https://github.com/MirakuSan" target="_blank" className="footer-icon">
                                <FontAwesomeIcon icon={faGithub} size="2x"/> Github
                            </a>
                            <a href="https://gitlab.com/MirakuSan" target="_blank">
                                <FontAwesomeIcon icon={faGitlab} size="2x"/> Gitlab
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}