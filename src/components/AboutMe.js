import React from "react";
import profilPicture from "../img/photo_profil.jpg"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faGithub, faGitlab, faLinkedin, faTwitter} from "@fortawesome/free-brands-svg-icons";

export default function AboutMe() {
    return (
        <div className="about-me">
            <div className="container py-5">
                <div className="row">
                    <div className="col-lg-6 col-xm-12">
                        <div className="photo-wrap mb-5">
                            <img src={profilPicture} alt="Ma photo de profil"
                                 className="img-profil rounded-circle img-fluid shadow-lg"/>
                        </div>
                    </div>
                    <div className="col-lg-6 col-xm-12">
                        <h1 className="text-uppercase about-heading">A propos de moi</h1>
                        <p className="about-text">
                            Hello ! Je m'appelle Damien Carrier. <br/>
                            Je suis actuellement apprentis ingénieur à l'IMT Mines d'Alès.<br/>
                            Je suis aussi développeur web Symfony chez ProwebCE depuis plus de deux ans maintenant.<br/><br/>
                            Fan de football, du FC Barcelone et de sport en général, je pratique le Handball depuis mes
                            12 ans. <br/>
                            Je pratique aussi occasionnellement l'escalade de bloc.
                        </p>
                        <div className="social-network">
                            <h2 className="text-uppercase about-heading"> Mes réseaux</h2>
                            <a href="https://twitter.com/MirakuSan" target="_blank">
                                <FontAwesomeIcon icon={faTwitter} size="3x"/>
                            </a>
                            <a href="https://www.linkedin.com/in/damien-carrier-b73005150/" target="_blank">
                                <FontAwesomeIcon icon={faLinkedin} size="3x"/>
                            </a>
                            <a href="https://github.com/MirakuSan" target="_blank">
                                <FontAwesomeIcon icon={faGithub} size="3x"/>
                            </a>
                            <a href="https://gitlab.com/MirakuSan" target="_blank">
                                <FontAwesomeIcon icon={faGitlab} size="3x"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}