import React from 'react';
import picture from '../img/photo_profil.jpg'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";

import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import Header from "./Header";
import AboutMe from "./AboutMe";
import CurriculumVitae from "./CurriculumVitae";
import Projects from "./Projects";

export default function Navbar() {
    return (
        <Router>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    <Link to="/" className="navbar-brand">
                        <img src={picture} alt="" className="logo rounded-circle img-fluid shadow-lg"/>
                        <span className="ms-2">damscrr.fr</span>
                    </Link>
                    <button className="navbar-toggler"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                    >
                        <FontAwesomeIcon icon={faBars} color={"#fff"}/>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item">
                                <Link to="/" className="nav-link">
                                    Accueil
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/about" className="nav-link">
                                    A propos de moi
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/cv" className="nav-link">
                                    Curriculum vitae
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/projects" className="nav-link">
                                    Mes projets
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <Switch>
                <Route path="/about">
                    <AboutMe/>
                </Route>
                <Route path="/cv">
                    <CurriculumVitae/>
                </Route>
                <Route path="/projects">
                    <Projects/>
                </Route>
                <Route path="/">
                    <Header/>
                </Route>
            </Switch>
        </Router>
    );
}