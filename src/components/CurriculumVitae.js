import React from "react";

export default function CurriculumVitae() {
    return (
        <div className="container-fluid px-5">
            <div className="row">
                <div id="experience" className="experience col">
                    <div className="d-flex justify-content-center my-5 experience-title">
                        <h1>Experiences professionnelles</h1>
                    </div>
                    <div className="container experience-wrapper">
                        <div className="timeline-block timeline-block-right">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Depuis septembre 2019</h3>
                                <h4>Apprentis Ingénieur - Développement logiciel</h4>
                                <p>
                                    ProwebCE - Saint Martin d'Hères (38) <br/>
                                    Poste de Développeur Web Symfony <br/> <br/>
                                    <span className="fw-bold">Première année : </span> Seul à gérer le développement et
                                    la maintenance du projet
                                    EMYS. <br/>
                                    Application interne de stockage d'informations pour les données importantes liées
                                    aux applications clientes. <br/> <br/>
                                    <span className="fw-bold">Deuxième année : </span> Intégration d'une équipe sur le
                                    projet V12 - Application Cliente de
                                    gestion pour les CSE d'entreprise. <br/>
                                    Développement de nouvelles fonctionnalité, préparation de l'application pour la
                                    migration de Symfony 3.4 à 4.4, correction d'anomalies, développement de tests
                                    BEHAT, refonte de fonctionnalités existantes.
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-left">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Mars 2019 - Juillet 2019</h3>
                                <h4>Développeur Web Symfony</h4>
                                <p>
                                    ProwebCE - Saint Martin d'Hères (38) <br/> <br/>
                                    Intégration d'une équipe sur le projet V12 - Application Cliente de gestion pour les
                                    CSE d'entreprise. <br/>
                                    Développement de nouvelles fonctionnalité, correction d'anomalies, refonte de
                                    fonctionnalités existantes.
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-right">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Novembre 2018 - Janvier 2019</h3>
                                <h4>Stagiaire Développeur Web</h4>
                                <p>
                                    ProwebCE - Saint Martin d'Hères (38) <br/>
                                    Poste de Stagiaire Développeur Web Symfony <br/> <br/>
                                    Seul à gérer le développement et la maintenance du projet EMYS. <br/>
                                    Développement Web Symfony 2.4 puis 3.4 <br/>
                                    Application interne de stockage d'informations pour les données importantes liées
                                    aux applications clientes.
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-left">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Mars 2018 - Juin 2018</h3>
                                <h4>Co-Chef de projet & Chargé de communication - Projet Aquatica</h4>
                                <p>
                                    Développement d'une application mobile ludo-éducation pour l'apprentissage des
                                    mathématiques et de l'informatique à l'usage d'élèves de classes de Troisième en
                                    équipe de 8. <br/>
                                    Communication et Tests de l'application avec une classe de Troisième du lycée
                                    Stendhal (Grenoble 38) <br/>
                                    Rédaction d'un rapport de projet <br/>
                                    Gestion des tâches de l'équipe.
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-right">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Janvier 2018 - Février 2018</h3>
                                <h4>Chef de projet - Projet : l'Îles Interdites</h4>
                                <p>
                                    Programmation en JAVA d'un jeu de plateau: l'Îles interdite
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="experience" className="experience col">
                    <div className="d-flex justify-content-center my-5 experience-title">
                        <h1>Diplômes & Formations</h1>
                    </div>
                    <div className="container experience-wrapper">
                        <div className="timeline-block timeline-block-right">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Depuis septembre 2019</h3>
                                <h4>Apprentis Ingénieur - Développement logiciel</h4>
                                <p>
                                    IMT Mines d'Alès - Alès (30) <br/>
                                    Trois ans de parcours ingénieur avec spécialisation dans le développement logiciel
                                    en deuxième année.
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-left">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Février 2017 - Janvier 2019</h3>
                                <h4>DUT Informatique en semestre décalé</h4>
                                <p>
                                    IUT2 Grenoble - Université Grenoble Alpes - Grenoble (38)<br/>
                                    DUT Informatique en deux ans, en semestre décalé.
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-right">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Septembre 2016 - Janvier 2017</h3>
                                <h4>Licence 1 - Informatique et Mathématiques appliquées - Parcours International.</h4>
                                <p>
                                    DLST - Université Grenoble Alpes - Grenoble (38)<br/>
                                    Premier semestre de Licence 1 option Informatique et Mathématiques appliquées en
                                    parcours international (La moitié des cours dispensés en anglais).
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-left">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Septembre 2013 - Juin 2016</h3>
                                <h4>Classe Préparatoire aux Grandes Écoles - Technologie et Sciences Industrielles</h4>
                                <p>
                                    Lycée Monge - Chambéry (73)<br/>
                                    CPGE TSI en 3 ans (5/2) option Technologie et Sciences Industrielles
                                </p>
                            </div>
                        </div>
                        {/* - */}
                        <div className="timeline-block timeline-block-right">
                            <div className="marker"/>
                            <div className="timeline-content">
                                <h3>Septembre 2009 - Juin 2013</h3>
                                <h4>Baccalauréat Sciences et Techniques de l'Industrie et du Développement Durable -
                                    Énergie Environnement</h4>
                                <p>
                                    <br/>
                                    École des Métiers de l'Énergie Paul Louis Merlin - Saint Martin d'Hères (38)<br/>
                                    Baccalauréat STI 2D option Énergie et Environnement avec mention Bien <br/>
                                    Obtention du BEP Électrotechnique en seconde mention Bien.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}